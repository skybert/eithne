
// global member variables
var mPlayList = new Array();
var mSongByHref = new Map();

function addToPlayList(song) {
  playList().push(song.href);
  mSongByHref.set(song.href, song);
}

function playList() {
  return mPlayList;
}

function song(name, album, artist, href) {
  return {
    "name": name,
    "artist": artist,
    "album": album,
    "href": href
  };
}

function initAudioPlayer() {
  // printPlayList();

  var audio;
  var playlist;
  var tracks;
  var current;

  var player = document.getElementById('audio');
  player.addEventListener(
    "ended",
    function() {
      play(nextSong(player.src).href);
    },
    true
  );
}

function play(link) {
  player = document.getElementById('audio');
  now = document.getElementById('now-playing');
  let song = mSongByHref.get(link);
  now.innerHTML = "Now playing: " + song.name + " by " + song.artist;
  player.src = link;
  player.play();
}

function playNextSong() {
  var player = document.getElementById('audio');
  play(nextSong(player.src).href);
}

function nextSong(currentHref) {
  let remove = location.protocol +
    '//' +
    location.hostname +
    (location.port ? ':'+ location.port: '');

  let uri = currentHref.substring(remove.length);

  var nextIndex = 0;
  for (var i = 0; i < playList().length; i++) {
    if (playList()[i] == uri &&
        i < playList().length - 1) {
      nextIndex = i + 1;
    }
  }

  return mSongByHref.get(playList()[nextIndex]);
}

function printSong(song) {
  return "[name=" + song.name
       + " artist=" + song.artist
       + " href=" + song.href
  ;
}

function printPlayList() {
  for (var i = 0; i < playList().length; i++) {
    let href = playList()[i];
    console.log("i=" + i +
                " song=" + printSong(mSongByHref.get(href)) +
                " next=" + nextSong(href).href
               );
  }
}

