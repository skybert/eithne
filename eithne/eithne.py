#! /usr/bin/env python3

import os.path
import sys
import os

from flask import Flask, render_template
from flask import send_file

import mutagen


app = Flask(__name__, static_folder="static")
music_library = {}


def save_cache(dir, app):
    pass


@app.route("/files/<path:uri>")
def download_file(uri):
  # TODO check uri for ..'s, slashes and the like
  file = dir + uri
  return send_file(file)


@app.route("/")
def show_library():
    return render_template("library.html", library=music_library)


def init_music_library(dir):
    cache = []
    failed_files = []
    for root, dirs, files in os.walk(dir):
        for file in files:
            if not file.endswith(".ogg") and not file.endswith(".mp3"):
                continue

            file_with_path = os.path.join(root, file)
            try:
                song = mutagen.File(file_with_path)
            except:
                failed_files.append(file_with_path)

            if song and "artist" in song:
                artist = song["artist"][0]

                if artist in music_library:
                    artist_albums = music_library[artist]
                else:
                    artist_albums = {}
                    music_library[artist] = artist_albums

                if "album" in song:
                    album = song["album"][0]
                else:
                    album = "unknown"

                if "title" not in song:
                    continue

                s = {
                    "title": song["title"][0],
                    "file": file_with_path,
                    "href": file_with_path.replace(dir, "/files/")
                }
                if album in artist_albums:
                    artist_albums[album].append(s)
                else:
                    album_songs = []
                    album_songs.append(s)
                    artist_albums[album] = album_songs

    print("Found " + str(len(music_library)) + " artists in " + dir)
    print("I couldn't read " + str(len(failed_files)) + " files.")
    save_cache(dir, cache)


if __name__ == "__main__":
    if len(sys.argv) < 2:
        sys.exit(1)

    dir = sys.argv[1]

    if not os.path.isdir(dir):
        print(dir + " is not a dir!")
        sys.exit(1)

    init_music_library(dir)
    app.run(debug=True)
