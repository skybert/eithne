# Eithne Music Server


Start `eithne` and tell it where you music collection resides:
```
$ ./bin/eithne ~/music
```

You can now browse and listen to your music collection at
http://localhost:5000

That's it.

—Torstein

## Copying

Please do. See [LICENSE](LICENSE) for more information.


